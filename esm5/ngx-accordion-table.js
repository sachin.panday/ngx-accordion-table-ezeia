import { __extends } from 'tslib';
import { EventEmitter, Pipe, Component, Input, NgModule } from '@angular/core';
import { DomSanitizer, BrowserModule } from '@angular/platform-browser';

var Row = /** @class */ (function () {
    function Row(cells) {
        this.cells = cells;
    }
    Row.prototype.getCells = function () {
        return this.cells;
    };
    return Row;
}());
var Column = /** @class */ (function () {
    function Column(name) {
        this.name = name;
    }
    return Column;
}());
var TableColumn = /** @class */ (function (_super) {
    __extends(TableColumn, _super);
    function TableColumn(name, width) {
        var _this = _super.call(this, name) || this;
        _this.width = width;
        return _this;
    }
    TableColumn.prototype.allowHtml = function (allow) {
        this.htmlAllowed = true;
    };
    Object.defineProperty(TableColumn.prototype, "isHtmlAllowed", {
        get: function () {
            return this.htmlAllowed;
        },
        enumerable: true,
        configurable: true
    });
    return TableColumn;
}(Column));
var AccordionColumn = /** @class */ (function (_super) {
    __extends(AccordionColumn, _super);
    function AccordionColumn(name, width) {
        var _this = _super.call(this, name) || this;
        _this.width = width;
        return _this;
    }
    return AccordionColumn;
}(Column));
var TargetOpenAction = {
    ROW: 1,
    COLUMN: 2,
    ELEMENT: 3,
};
TargetOpenAction[TargetOpenAction.ROW] = "ROW";
TargetOpenAction[TargetOpenAction.COLUMN] = "COLUMN";
TargetOpenAction[TargetOpenAction.ELEMENT] = "ELEMENT";
var AccordionEventListener = /** @class */ (function () {
    function AccordionEventListener() {
        this.eventEmitters = new Map();
    }
    AccordionEventListener.prototype.notifyClickOn = function (row, column) {
        var key = column.name.toLowerCase();
        if (this.eventEmitters.has(key)) {
            var event = (this.eventEmitters.get(key));
            event.emit(row);
        }
    };
    AccordionEventListener.prototype.subscribe = function (columnName, callback) {
        var eventEmitter = new EventEmitter();
        eventEmitter.subscribe(callback);
        this.eventEmitters.set(columnName.toLowerCase(), eventEmitter);
    };
    return AccordionEventListener;
}());
var AccordionTemplate = /** @class */ (function () {
    function AccordionTemplate() {
        var chevronColumn = new TableColumn("", "50px");
        this.tableColumns = [chevronColumn];
        this.accordionColumns = [];
        this.interactiveColumn = "<span _ngcontent-c1 class=\"chevron-up\"></span>";
        this.targetOpenAction = TargetOpenAction.ROW;
        this.eventListener = new AccordionEventListener();
    }
    AccordionTemplate.prototype.addColumn = function (name, width) {
        this.tableColumns.push(new TableColumn(name, width));
        return this;
    };
    AccordionTemplate.prototype.addHiddenColumn = function (name) {
        var column = new TableColumn(name, '0px');
        column.hidden = true;
        this.tableColumns.push(column);
        return this;
    };
    AccordionTemplate.prototype.getTableColumnsForView = function () {
        return this.tableColumns;
    };
    AccordionTemplate.prototype.getTableColumns = function () {
        return this.tableColumns.slice(1, this.tableColumns.length);
    };
    AccordionTemplate.prototype.getInteractiveColumn = function () {
        return this.interactiveColumn;
    };
    AccordionTemplate.prototype.getAccordionColumns = function () {
        return this.accordionColumns;
    };
    AccordionTemplate.prototype.setInteractiveColumn = function (htmlTemplate) {
        this.interactiveColumn = htmlTemplate.replace(/(.*?)>(.*)/, '$1 _ngcontent-c1>$2');
    };
    AccordionTemplate.prototype.addAccordionColumn = function (name, width) {
        this.accordionColumns.push(new AccordionColumn(name, width));
        return this;
    };
    AccordionTemplate.prototype.addAccordionHiddenColumn = function (name) {
        var column = new AccordionColumn(name, '0px');
        column.hidden = true;
        this.accordionColumns.push(column);
        return this;
    };
    AccordionTemplate.prototype.addHtmlColumn = function (name, width) {
        var tableColumn = new TableColumn(name, width);
        tableColumn.allowHtml(true);
        this.tableColumns.push(tableColumn);
        return this;
    };
    AccordionTemplate.prototype.setTargetOpenAction = function (action) {
        this.targetOpenAction = action;
    };
    AccordionTemplate.prototype.getTargetOpenAction = function () {
        return this.targetOpenAction;
    };
    return AccordionTemplate;
}());
var AccordionData = /** @class */ (function () {
    function AccordionData(template) {
        this.rows = [];
        this.accordionTemplate = template;
    }
    AccordionData.prototype.addRow = function (cells) {
        if (this.accordionTemplate.getTableColumns().length !== cells.length) {
            throw "Data not matching with template";
        }
        var tableRow = new TableRow(cells, this.accordionTemplate.getAccordionColumns());
        this.rows.push(tableRow);
        return tableRow;
    };
    AccordionData.prototype.isHtmlColumn = function (index) {
        var column = this.accordionTemplate.getTableColumns()[index];
        return column.isHtmlAllowed;
    };
    AccordionData.prototype.getRows = function () {
        return this.rows;
    };
    return AccordionData;
}());
var TableRow = /** @class */ (function (_super) {
    __extends(TableRow, _super);
    function TableRow(cells, accordionTemplate) {
        var _this = _super.call(this, cells) || this;
        _this.accordionTemplate = accordionTemplate;
        return _this;
    }
    TableRow.prototype.addAccordionRow = function (accordionCells) {
        if (!this.accordionRows) {
            this.accordionRows = [];
        }
        if (this.accordionTemplate.length !== accordionCells.length) {
            throw 'Accordion data not matching with template';
        }
        this.accordionRows.push(new AccordionRow(accordionCells));
    };
    TableRow.prototype.addAccordionRows = function (rows) {
        var that = this;
        rows.forEach(function (cells) {
            that.addAccordionRow(cells);
        });
    };
    TableRow.prototype.getAccordionRows = function () {
        return this.accordionRows;
    };
    return TableRow;
}(Row));
var AccordionRow = /** @class */ (function (_super) {
    __extends(AccordionRow, _super);
    function AccordionRow() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AccordionRow;
}(Row));
var EscapeHtmlPipe = /** @class */ (function () {
    function EscapeHtmlPipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    EscapeHtmlPipe.prototype.transform = function (content) {
        return this.sanitizer.bypassSecurityTrustHtml(content);
    };
    return EscapeHtmlPipe;
}());
EscapeHtmlPipe.decorators = [
    { type: Pipe, args: [{ name: 'keepHtml', pure: false },] },
];
EscapeHtmlPipe.ctorParameters = function () { return [
    { type: DomSanitizer, },
]; };
var NgxAccordionTableComponent = /** @class */ (function () {
    function NgxAccordionTableComponent() {
    }
    return NgxAccordionTableComponent;
}());
NgxAccordionTableComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngx-accordion-table',
                template: "<template class=\"targetOpenAction\" [innerHTML]=\"template.getTargetOpenAction()\"></template>\n<table width=\"100%\" class=\"ngx-accordion-table\">\n    <thead>\n        <th *ngFor=\"let column of template.getTableColumnsForView()\" width=\"{{column.width}}\"\n            [hidden]=\"column.hidden\">\n            {{column.name}}\n        </th>\n    </thead>\n    <tbody>\n        <ng-container *ngFor=\"let row of data.getRows()\">\n            <tr onclick=\"toggleSibling(this, 1)\">\n                <td class=\"chevron\" onclick=\"toggleSibling(this, 2)\">\n                    <span _ngcontent-c1 [innerHTML]=\"template.getInteractiveColumn() | keepHtml\"\n                          onclick=\"toggleSibling(this, 3)\">\n                    </span>\n                </td>\n                <ng-container *ngFor=\"let cell of row.getCells(); let i = index;\">\n                    <td *ngIf=\"data.isHtmlColumn(i)\" [hidden]=\"template.getTableColumns()[i].hidden\">\n                        <ngx-clickable-cell [eventListener]=\"template.eventListener\"\n                                            [isHtml]=\"true\"\n                                            [value]=\"cell\"\n                                            [row]=\"row\"\n                                            [column]=\"template.getTableColumns()[i]\"></ngx-clickable-cell>\n                    </td>\n                    <td *ngIf=\"!data.isHtmlColumn(i)\" [hidden]=\"template.getTableColumns()[i].hidden\">\n                        <ngx-clickable-cell [eventListener]=\"template.eventListener\"\n                                            [value]=\"cell\"\n                                            [row]=\"row\"\n                                            [column]=\"template.getTableColumns()[i]\"\n                        ></ngx-clickable-cell>\n                    </td>\n                </ng-container>\n            </tr>\n            <tr class=\"extra\">\n                <td colspan=\"5\" style=\"padding-left:80px;\">\n                    <table class=\"ngx-accordion-accordion\" width=\"100%\">\n                        <thead>\n                            <th *ngFor=\"let accordionColumn of template.getAccordionColumns()\" width=\"{{accordionColumn.width}}\"\n                                [hidden]=\"accordionColumn.hidden\">\n                                {{accordionColumn.name}}\n                            </th>\n                        </thead>\n                        <tbody>\n                            <ng-container *ngFor=\"let accordionRow of row.getAccordionRows()\">\n                                <tr>\n                                    <ng-container *ngFor=\"let accordionCell of accordionRow.cells; let y = index;\">\n                                        <td [hidden]=\"template.getAccordionColumns()[y].hidden\">\n                                            <ngx-clickable-cell [eventListener]=\"template.eventListener\"\n                                                                [value]=\"accordionCell\"\n                                                                [row]=\"row\"\n                                                                [column]=\"template.getAccordionColumns()[y]\"></ngx-clickable-cell>\n                                        </td>\n                                    </ng-container>\n                                </tr>\n                            </ng-container>\n                        </tbody>\n                    </table>\n                </td>\n            </tr>\n        </ng-container>\n    </tbody>\n</table>",
                styles: [".extra{display:none}th{color:#121212;font-weight:700}td{color:#717171;border-bottom:1px solid rgba(234,234,234,.5)}td,th{padding:10px;text-align:left;font-size:14px}thead{border-bottom:1px solid #dfdfdf}.chevron-up{display:block;-webkit-box-sizing:border-box;box-sizing:border-box;width:22px;height:15px;max-width:55px;background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAPCAYAAADgbT9oAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABAhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ1dWlkOjY1RTYzOTA2ODZDRjExREJBNkUyRDg4N0NFQUNCNDA3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjFEQzdDNjI1ODU0NjExRTU4RTQwRkQwODFEOUZEMEE3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjFEQzdDNjI0ODU0NjExRTU4RTQwRkQwODFEOUZEMEE3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE1IChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MTk5NzA1OGEtZDI3OC00NDZkLWE4ODgtNGM4MGQ4YWI1NzNmIiBzdFJlZjpkb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6YzRkZmQxMGMtY2NlNS0xMTc4LWE5OGQtY2NkZmM5ODk5YWYwIi8+IDxkYzp0aXRsZT4gPHJkZjpBbHQ+IDxyZGY6bGkgeG1sOmxhbmc9IngtZGVmYXVsdCI+Z2x5cGhpY29uczwvcmRmOmxpPiA8L3JkZjpBbHQ+IDwvZGM6dGl0bGU+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+wam0HgAAAJRJREFUeNqck9ENwCAIRGGDjtSROpKjdISO0g0oJJKIMfSQ5D5UeFHhSETIpdFUj+oY9zNZbq9pYX+CShcEH6Be1wJ4gkLwBTTALa7FYQpPoK4LSQpwNL+WXLgEFW+Cv6z4d3AvaKMxWIOLowSPJG3MKTbnRWfBzqQN20J2Z7dfFsxs8LsvT615f2sQ8AAnBGrxCTAAYw3p0OmNW5kAAAAASUVORK5CYII=)}.chevron-up:hover{-webkit-transform:rotate(-90deg);transform:rotate(-90deg);-webkit-transition:all .1s linear 0s;transition:all .1s linear 0s}.chevron-up-undo{-webkit-transform:rotate(-180deg);transform:rotate(-180deg);-webkit-transition:all .1s linear 0s;transition:all .1s linear 0s}.chevron{width:5px;cursor:pointer}.table-foot{display:inline;float:right}.grid-follow-up{margin:auto;padding-left:200px}.grid-follow-up thead{border-bottom:none}.grid-follow-up tbody{background-color:rgba(244,255,255,.5)}.rotating{-webkit-animation:1s linear paused rotating;animation:1s linear paused rotating}"]
            },] },
];
NgxAccordionTableComponent.ctorParameters = function () { return []; };
NgxAccordionTableComponent.propDecorators = {
    "template": [{ type: Input },],
    "data": [{ type: Input },],
};
var NgxClickableCellComponent = /** @class */ (function () {
    function NgxClickableCellComponent() {
    }
    NgxClickableCellComponent.prototype.fireCellClick = function (event) {
        this.eventListener.notifyClickOn(this.row, this.column);
    };
    return NgxClickableCellComponent;
}());
NgxClickableCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngx-clickable-cell',
                template: "<ng-container *ngIf=\"isHtml\">\n    <span (click)=\"fireCellClick($event)\" [innerHTML]=\"value | keepHtml\">\n    </span>\n</ng-container>\n<ng-container *ngIf=\"!isHtml\">\n    <span (click)=\"fireCellClick($event)\">\n        {{value}}\n    </span>\n</ng-container>",
                styles: [""]
            },] },
];
NgxClickableCellComponent.ctorParameters = function () { return []; };
NgxClickableCellComponent.propDecorators = {
    "buttonId": [{ type: Input },],
    "value": [{ type: Input },],
    "eventListener": [{ type: Input },],
    "isHtml": [{ type: Input },],
    "column": [{ type: Input },],
    "row": [{ type: Input },],
};
var NgxAccordionTableModule = /** @class */ (function () {
    function NgxAccordionTableModule() {
    }
    return NgxAccordionTableModule;
}());
NgxAccordionTableModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    NgxAccordionTableComponent,
                    EscapeHtmlPipe,
                    NgxClickableCellComponent
                ],
                exports: [
                    NgxAccordionTableComponent
                ],
                imports: [
                    BrowserModule,
                ]
            },] },
];
NgxAccordionTableModule.ctorParameters = function () { return []; };

export { NgxAccordionTableModule, Row, Column, TableColumn, AccordionColumn, TargetOpenAction, AccordionEventListener, AccordionTemplate, AccordionData, TableRow, AccordionRow, EscapeHtmlPipe, NgxAccordionTableComponent as ɵa, NgxClickableCellComponent as ɵb };
//# sourceMappingURL=ngx-accordion-table.js.map
