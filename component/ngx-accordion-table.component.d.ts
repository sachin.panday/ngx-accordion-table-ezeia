import { AccordionTemplate, AccordionData } from './ngx-accordion-template';
export declare class NgxAccordionTableComponent {
    template: AccordionTemplate;
    data: AccordionData;
    constructor();
}
