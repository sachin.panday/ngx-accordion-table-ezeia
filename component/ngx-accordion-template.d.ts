import { PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
export { SafeHtml };
export declare class Row {
    private cells;
    constructor(cells: Array<String>);
    getCells(): String[];
}
export declare class Column {
    name: string;
    width: string;
    hidden: boolean;
    constructor(name: any);
}
export declare class TableColumn extends Column {
    private htmlAllowed;
    constructor(name: any, width: any);
    allowHtml(allow: boolean): void;
    readonly isHtmlAllowed: boolean;
}
export declare class AccordionColumn extends Column {
    constructor(name: any, width: any);
}
export declare enum TargetOpenAction {
    ROW = 1,
    COLUMN = 2,
    ELEMENT = 3,
}
export declare class AccordionEventListener {
    private eventEmitters;
    constructor();
    notifyClickOn(row: TableRow, column: Column): void;
    subscribe(columnName: string, callback: any): void;
}
export declare class AccordionTemplate {
    private accordionColumns;
    private tableColumns;
    private interactiveColumn;
    private targetOpenAction;
    eventListener: AccordionEventListener;
    constructor();
    /**
     * Builder method to add columns to table model
     */
    addColumn(name: string, width: string): this;
    addHiddenColumn(name: string): this;
    /**
     * Returns all columns inclusively columns added automatically
     */
    getTableColumnsForView(): TableColumn[];
    /**
     * Returns all columns added manually
     */
    getTableColumns(): TableColumn[];
    /**
     * Return HTML from interactive column
     */
    getInteractiveColumn(): string;
    /**
     * Returns accordion columns added
     */
    getAccordionColumns(): AccordionColumn[];
    /**
     * Changes first column of table with html template received, allow to implement your own image, or icon o additional behaviour.
     */
    setInteractiveColumn(htmlTemplate: string): void;
    /**
     * Builder method to add columns to accordion model
     */
    addAccordionColumn(name: string, width: string): this;
    /**
     * Builder method to add hidden column to accordion model
     */
    addAccordionHiddenColumn(name: string): this;
    /**
     * Add a column which is allowed to render HTML
     */
    addHtmlColumn(name: string, width: string): this;
    /**
     * Changes if accordion table will opened by click on element, row or column
     */
    setTargetOpenAction(action: TargetOpenAction): void;
    /**
     * Return target option specified
     */
    getTargetOpenAction(): TargetOpenAction;
}
export declare class AccordionData {
    private accordionTemplate;
    private rows;
    constructor(template: AccordionTemplate);
    /**
     * Add a table row to accordion and return row created to add accordion data to table
     */
    addRow(cells: any): TableRow;
    isHtmlColumn(index: any): boolean;
    getRows(): TableRow[];
}
export declare class TableRow extends Row {
    private accordionRows;
    private accordionTemplate;
    constructor(cells: any, accordionTemplate: Array<AccordionColumn>);
    /**
     * Add row to child table of this row;
     */
    addAccordionRow(accordionCells: Array<String>): void;
    addAccordionRows(rows: Array<Array<String>>): void;
    getAccordionRows(): AccordionRow[];
}
export declare class AccordionRow extends Row {
}
export declare class EscapeHtmlPipe implements PipeTransform {
    private sanitizer;
    constructor(sanitizer: DomSanitizer);
    transform(content: any): SafeHtml;
}
