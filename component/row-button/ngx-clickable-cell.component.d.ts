import { AccordionEventListener, Column, TableRow } from '../ngx-accordion-template';
export declare class NgxClickableCellComponent {
    buttonId: string;
    value: string;
    eventListener: AccordionEventListener;
    isHtml: boolean;
    column: Column;
    row: TableRow;
    constructor();
    fireCellClick(event: any): void;
}
