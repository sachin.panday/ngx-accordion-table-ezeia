import { EventEmitter, Pipe, Component, Input, NgModule } from '@angular/core';
import { DomSanitizer, BrowserModule } from '@angular/platform-browser';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class Row {
    /**
     * @param {?} cells
     */
    constructor(cells) {
        this.cells = cells;
    }
    /**
     * @return {?}
     */
    getCells() {
        return this.cells;
    }
}
class Column {
    /**
     * @param {?} name
     */
    constructor(name) {
        this.name = name;
    }
}
class TableColumn extends Column {
    /**
     * @param {?} name
     * @param {?} width
     */
    constructor(name, width) {
        super(name);
        this.width = width;
    }
    /**
     * @param {?} allow
     * @return {?}
     */
    allowHtml(allow) {
        this.htmlAllowed = true;
    }
    /**
     * @return {?}
     */
    get isHtmlAllowed() {
        return this.htmlAllowed;
    }
}
class AccordionColumn extends Column {
    /**
     * @param {?} name
     * @param {?} width
     */
    constructor(name, width) {
        super(name);
        this.width = width;
    }
}
/** @enum {number} */
const TargetOpenAction = {
    ROW: 1,
    COLUMN: 2,
    ELEMENT: 3,
};
TargetOpenAction[TargetOpenAction.ROW] = "ROW";
TargetOpenAction[TargetOpenAction.COLUMN] = "COLUMN";
TargetOpenAction[TargetOpenAction.ELEMENT] = "ELEMENT";
class AccordionEventListener {
    constructor() {
        this.eventEmitters = new Map();
    }
    /**
     * @param {?} row
     * @param {?} column
     * @return {?}
     */
    notifyClickOn(row, column) {
        let /** @type {?} */ key = column.name.toLowerCase();
        if (this.eventEmitters.has(key)) {
            let /** @type {?} */ event = /** @type {?} */ (this.eventEmitters.get(key));
            event.emit(row);
        }
    }
    /**
     * @param {?} columnName
     * @param {?} callback
     * @return {?}
     */
    subscribe(columnName, callback) {
        let /** @type {?} */ eventEmitter = new EventEmitter();
        eventEmitter.subscribe(callback);
        this.eventEmitters.set(columnName.toLowerCase(), eventEmitter);
    }
}
class AccordionTemplate {
    constructor() {
        let /** @type {?} */ chevronColumn = new TableColumn("", "50px");
        this.tableColumns = [chevronColumn];
        this.accordionColumns = [];
        this.interactiveColumn = `<span _ngcontent-c1 class="chevron-up"></span>`;
        this.targetOpenAction = TargetOpenAction.ROW;
        this.eventListener = new AccordionEventListener();
    }
    /**
     * Builder method to add columns to table model
     * @param {?} name
     * @param {?} width
     * @return {?}
     */
    addColumn(name, width) {
        this.tableColumns.push(new TableColumn(name, width));
        return this;
    }
    /**
     * @param {?} name
     * @return {?}
     */
    addHiddenColumn(name) {
        let /** @type {?} */ column = new TableColumn(name, '0px');
        column.hidden = true;
        this.tableColumns.push(column);
        return this;
    }
    /**
     * Returns all columns inclusively columns added automatically
     * @return {?}
     */
    getTableColumnsForView() {
        return this.tableColumns;
    }
    /**
     * Returns all columns added manually
     * @return {?}
     */
    getTableColumns() {
        return this.tableColumns.slice(1, this.tableColumns.length);
    }
    /**
     * Return HTML from interactive column
     * @return {?}
     */
    getInteractiveColumn() {
        return this.interactiveColumn;
    }
    /**
     * Returns accordion columns added
     * @return {?}
     */
    getAccordionColumns() {
        return this.accordionColumns;
    }
    /**
     * Changes first column of table with html template received, allow to implement your own image, or icon o additional behaviour.
     * @param {?} htmlTemplate
     * @return {?}
     */
    setInteractiveColumn(htmlTemplate) {
        this.interactiveColumn = htmlTemplate.replace(/(.*?)>(.*)/, '$1 _ngcontent-c1>$2');
    }
    /**
     * Builder method to add columns to accordion model
     * @param {?} name
     * @param {?} width
     * @return {?}
     */
    addAccordionColumn(name, width) {
        this.accordionColumns.push(new AccordionColumn(name, width));
        return this;
    }
    /**
     * Builder method to add hidden column to accordion model
     * @param {?} name
     * @return {?}
     */
    addAccordionHiddenColumn(name) {
        let /** @type {?} */ column = new AccordionColumn(name, '0px');
        column.hidden = true;
        this.accordionColumns.push(column);
        return this;
    }
    /**
     * Add a column which is allowed to render HTML
     * @param {?} name
     * @param {?} width
     * @return {?}
     */
    addHtmlColumn(name, width) {
        let /** @type {?} */ tableColumn = new TableColumn(name, width);
        tableColumn.allowHtml(true);
        this.tableColumns.push(tableColumn);
        return this;
    }
    /**
     * Changes if accordion table will opened by click on element, row or column
     * @param {?} action
     * @return {?}
     */
    setTargetOpenAction(action) {
        this.targetOpenAction = action;
    }
    /**
     * Return target option specified
     * @return {?}
     */
    getTargetOpenAction() {
        return this.targetOpenAction;
    }
}
class AccordionData {
    /**
     * @param {?} template
     */
    constructor(template) {
        this.rows = [];
        this.accordionTemplate = template;
    }
    /**
     * Add a table row to accordion and return row created to add accordion data to table
     * @param {?} cells
     * @return {?}
     */
    addRow(cells) {
        if (this.accordionTemplate.getTableColumns().length !== cells.length) {
            throw "Data not matching with template";
        }
        let /** @type {?} */ tableRow = new TableRow(cells, this.accordionTemplate.getAccordionColumns());
        this.rows.push(tableRow);
        return tableRow;
    }
    /**
     * @param {?} index
     * @return {?}
     */
    isHtmlColumn(index) {
        let /** @type {?} */ column = this.accordionTemplate.getTableColumns()[index];
        return column.isHtmlAllowed;
    }
    /**
     * @return {?}
     */
    getRows() {
        return this.rows;
    }
}
class TableRow extends Row {
    /**
     * @param {?} cells
     * @param {?} accordionTemplate
     */
    constructor(cells, accordionTemplate) {
        super(cells);
        this.accordionTemplate = accordionTemplate;
    }
    /**
     * Add row to child table of this row;
     * @param {?} accordionCells
     * @return {?}
     */
    addAccordionRow(accordionCells) {
        if (!this.accordionRows) {
            this.accordionRows = [];
        }
        if (this.accordionTemplate.length !== accordionCells.length) {
            throw 'Accordion data not matching with template';
        }
        this.accordionRows.push(new AccordionRow(accordionCells));
    }
    /**
     * @param {?} rows
     * @return {?}
     */
    addAccordionRows(rows) {
        let /** @type {?} */ that = this;
        rows.forEach(function (cells) {
            that.addAccordionRow(cells);
        });
    }
    /**
     * @return {?}
     */
    getAccordionRows() {
        return this.accordionRows;
    }
}
class AccordionRow extends Row {
}
class EscapeHtmlPipe {
    /**
     * @param {?} sanitizer
     */
    constructor(sanitizer) {
        this.sanitizer = sanitizer;
    }
    /**
     * @param {?} content
     * @return {?}
     */
    transform(content) {
        return this.sanitizer.bypassSecurityTrustHtml(content);
    }
}
EscapeHtmlPipe.decorators = [
    { type: Pipe, args: [{ name: 'keepHtml', pure: false },] },
];
/** @nocollapse */
EscapeHtmlPipe.ctorParameters = () => [
    { type: DomSanitizer, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NgxAccordionTableComponent {
    constructor() { }
}
NgxAccordionTableComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngx-accordion-table',
                template: `<template class="targetOpenAction" [innerHTML]="template.getTargetOpenAction()"></template>
<table width="100%" class="ngx-accordion-table">
    <thead>
        <th *ngFor="let column of template.getTableColumnsForView()" width="{{column.width}}"
            [hidden]="column.hidden">
            {{column.name}}
        </th>
    </thead>
    <tbody>
        <ng-container *ngFor="let row of data.getRows()">
            <tr onclick="toggleSibling(this, 1)">
                <td class="chevron" onclick="toggleSibling(this, 2)">
                    <span _ngcontent-c1 [innerHTML]="template.getInteractiveColumn() | keepHtml"
                          onclick="toggleSibling(this, 3)">
                    </span>
                </td>
                <ng-container *ngFor="let cell of row.getCells(); let i = index;">
                    <td *ngIf="data.isHtmlColumn(i)" [hidden]="template.getTableColumns()[i].hidden">
                        <ngx-clickable-cell [eventListener]="template.eventListener"
                                            [isHtml]="true"
                                            [value]="cell"
                                            [row]="row"
                                            [column]="template.getTableColumns()[i]"></ngx-clickable-cell>
                    </td>
                    <td *ngIf="!data.isHtmlColumn(i)" [hidden]="template.getTableColumns()[i].hidden">
                        <ngx-clickable-cell [eventListener]="template.eventListener"
                                            [value]="cell"
                                            [row]="row"
                                            [column]="template.getTableColumns()[i]"
                        ></ngx-clickable-cell>
                    </td>
                </ng-container>
            </tr>
            <tr class="extra">
                <td colspan="5" style="padding-left:80px;">
                    <table class="ngx-accordion-accordion" width="100%">
                        <thead>
                            <th *ngFor="let accordionColumn of template.getAccordionColumns()" width="{{accordionColumn.width}}"
                                [hidden]="accordionColumn.hidden">
                                {{accordionColumn.name}}
                            </th>
                        </thead>
                        <tbody>
                            <ng-container *ngFor="let accordionRow of row.getAccordionRows()">
                                <tr>
                                    <ng-container *ngFor="let accordionCell of accordionRow.cells; let y = index;">
                                        <td [hidden]="template.getAccordionColumns()[y].hidden">
                                            <ngx-clickable-cell [eventListener]="template.eventListener"
                                                                [value]="accordionCell"
                                                                [row]="row"
                                                                [column]="template.getAccordionColumns()[y]"></ngx-clickable-cell>
                                        </td>
                                    </ng-container>
                                </tr>
                            </ng-container>
                        </tbody>
                    </table>
                </td>
            </tr>
        </ng-container>
    </tbody>
</table>`,
                styles: [`.extra{display:none}th{color:#121212;font-weight:700}td{color:#717171;border-bottom:1px solid rgba(234,234,234,.5)}td,th{padding:10px;text-align:left;font-size:14px}thead{border-bottom:1px solid #dfdfdf}.chevron-up{display:block;-webkit-box-sizing:border-box;box-sizing:border-box;width:22px;height:15px;max-width:55px;background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAPCAYAAADgbT9oAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABAhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ1dWlkOjY1RTYzOTA2ODZDRjExREJBNkUyRDg4N0NFQUNCNDA3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjFEQzdDNjI1ODU0NjExRTU4RTQwRkQwODFEOUZEMEE3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjFEQzdDNjI0ODU0NjExRTU4RTQwRkQwODFEOUZEMEE3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE1IChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MTk5NzA1OGEtZDI3OC00NDZkLWE4ODgtNGM4MGQ4YWI1NzNmIiBzdFJlZjpkb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6YzRkZmQxMGMtY2NlNS0xMTc4LWE5OGQtY2NkZmM5ODk5YWYwIi8+IDxkYzp0aXRsZT4gPHJkZjpBbHQ+IDxyZGY6bGkgeG1sOmxhbmc9IngtZGVmYXVsdCI+Z2x5cGhpY29uczwvcmRmOmxpPiA8L3JkZjpBbHQ+IDwvZGM6dGl0bGU+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+wam0HgAAAJRJREFUeNqck9ENwCAIRGGDjtSROpKjdISO0g0oJJKIMfSQ5D5UeFHhSETIpdFUj+oY9zNZbq9pYX+CShcEH6Be1wJ4gkLwBTTALa7FYQpPoK4LSQpwNL+WXLgEFW+Cv6z4d3AvaKMxWIOLowSPJG3MKTbnRWfBzqQN20J2Z7dfFsxs8LsvT615f2sQ8AAnBGrxCTAAYw3p0OmNW5kAAAAASUVORK5CYII=)}.chevron-up:hover{-webkit-transform:rotate(-90deg);transform:rotate(-90deg);-webkit-transition:all .1s linear 0s;transition:all .1s linear 0s}.chevron-up-undo{-webkit-transform:rotate(-180deg);transform:rotate(-180deg);-webkit-transition:all .1s linear 0s;transition:all .1s linear 0s}.chevron{width:5px;cursor:pointer}.table-foot{display:inline;float:right}.grid-follow-up{margin:auto;padding-left:200px}.grid-follow-up thead{border-bottom:none}.grid-follow-up tbody{background-color:rgba(244,255,255,.5)}.rotating{-webkit-animation:1s linear paused rotating;animation:1s linear paused rotating}`]
            },] },
];
/** @nocollapse */
NgxAccordionTableComponent.ctorParameters = () => [];
NgxAccordionTableComponent.propDecorators = {
    "template": [{ type: Input },],
    "data": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NgxClickableCellComponent {
    constructor() { }
    /**
     * @param {?} event
     * @return {?}
     */
    fireCellClick(event) {
        this.eventListener.notifyClickOn(this.row, this.column);
    }
}
NgxClickableCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngx-clickable-cell',
                template: `<ng-container *ngIf="isHtml">
    <span (click)="fireCellClick($event)" [innerHTML]="value | keepHtml">
    </span>
</ng-container>
<ng-container *ngIf="!isHtml">
    <span (click)="fireCellClick($event)">
        {{value}}
    </span>
</ng-container>`,
                styles: [``]
            },] },
];
/** @nocollapse */
NgxClickableCellComponent.ctorParameters = () => [];
NgxClickableCellComponent.propDecorators = {
    "buttonId": [{ type: Input },],
    "value": [{ type: Input },],
    "eventListener": [{ type: Input },],
    "isHtml": [{ type: Input },],
    "column": [{ type: Input },],
    "row": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NgxAccordionTableModule {
}
NgxAccordionTableModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    NgxAccordionTableComponent,
                    EscapeHtmlPipe,
                    NgxClickableCellComponent
                ],
                exports: [
                    NgxAccordionTableComponent
                ],
                imports: [
                    BrowserModule,
                ]
            },] },
];
/** @nocollapse */
NgxAccordionTableModule.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Generated bundle index. Do not edit.
 */

export { NgxAccordionTableModule, Row, Column, TableColumn, AccordionColumn, TargetOpenAction, AccordionEventListener, AccordionTemplate, AccordionData, TableRow, AccordionRow, EscapeHtmlPipe, NgxAccordionTableComponent as ɵa, NgxClickableCellComponent as ɵb };
//# sourceMappingURL=ngx-accordion-table.js.map
